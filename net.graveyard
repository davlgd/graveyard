format = unwritten-1

app-admin/
    mime-support/
        :0 3.64
            comment = Replaced by app-misc/mailcap
            commit-id = ebacb542
            description = MIME files mime.types & mailcap and support programs
            homepage = https://packages.debian.org/sid/mime-support
            removed-by = Timo Gurr <tgurr@exherbo.org>
            removed-from = net
    puppet/
        :0 3.6.2
            comment = Unmaintained
            commit-id = 5f5ee26a3c1c
            description = an automated administrative engine for your Linux and Unix systems
            homepage = http://puppetlabs.com/
            removed-by = Marvin Schmidt <marv@exherbo.org>
            removed-from = net
app-crypt/
    heimdal/
        :0 1.5.3-r9
            comment = Outdated with known security issues
            commit-id = 5e4e5082
            description = Swedish implementation of Kerberos 5
            homepage = https://github.com/heimdal/
            removed-by = Heiko Becker <heirecka@exherbo.org>
            removed-from = net
dev-db/
    ntdb/
        :0 1.0
            commit-id = aabc088
            description = Simple database API - Version 2 (formerly tdb2)
            homepage = https://www.samba.org/ftp/ntdb
            removed-by = Timo Gurr <tgurr@exherbo.org>
            removed-from = net
dev-libs/
    libquvi/
        :0 0.9.4
            comment = Dead upstream, last release from 2013
            commit-id = c3bcf3d
            description = A small C library that can be used to parse flash media stream URLs
            homepage = http://quvi.sourceforge.net/
            removed-by = Timo Gurr <tgurr@exherbo.org>
            removed-from = net
    libquvi-scripts/
        :0 0.9.20131130
            comment = Dead upstream, last release from 2013
            commit-id = 54fcd3e
            description = Scripts used by libquvi to parse media stream URLs
            homepage = http://quvi.sourceforge.net/
            removed-by = Timo Gurr <tgurr@exherbo.org>
            removed-from = net
    quvi/
        :0 0.9.5
            comment = Dead upstream, last release from 2013
            commit-id = 3efc1ee
            description = Flash video download link parser
            homepage = http://quvi.sourceforge.net/
            removed-by = Timo Gurr <tgurr@exherbo.org>
            removed-from = net
mail-filter/
    dspam/
        :0 3.10.2-r2
            comment = Appears abandoned, fails to build with gcc >= 10
            commit-id = d32f66dc
            description = A scalable and open-source content-based spam filter designed for multi-user enterprise systems
            homepage = https://dspam.sourceforge.net/
            removed-by = Heiko Becker <heirecka@exherbo.org>
            removed-from = net
net/
    aiccu/
        :0 20070115-r1
            comment = Dead upstream, last release on January 2007
            commit-id = d2f9294f
            description = Automatic IPv6 Connectivity Client Utility
            homepage = http://www.sixxs.net/tools/aiccu
            removed-by = Dolan Dolan <vanishingdreams@tutanota.com>
            removed-from = net
    gogs/
        :0 0.11.91
            comment = unmaintained on Exherbo
            commit-id = ce23a1f
            description = A painless self-hosted Git service
            homepage = https://github.com/gogs/gogs
            removed-by = Timo Gurr <tgurr@exherbo.org>
            removed-from = net
    mxisd/
        :0 1.4.4
            comment = discontinued, use ma1sd instead
            commit-id = cdeaa96
            description = mxisd - Federated Matrix Identity Server
            homepage = https://github.com/kamax-matrix/mxisd
            removed-by = Timo Gurr <tgurr@exherbo.org>
            removed-from = net
    openhab-binding-loxone/
        :0 2.2.0.3
            comment = openhab-binding-loxone has been merged into the official openhab-addons package with openHAB 2.2.0
            commit-id = da12668821844c2860334014c7f9de35e7450740
            description = This binding integrates Loxone Miniserver with openHAB
            homepage = https://github.com/ppieczul/org.openhab.binding.loxone
            removed-by = Timo Gurr <tgurr@exherbo.org>
            removed-from = net
net-analyzer/
    journalbeat/
        :0 7.13.0
            comment = discontinued, replaced by filebeat
            commit-id = 7619833
            description = Ships systemd journal entries to Elasticsearch or Logstash
            homepage = https://www.elastic.co/guide/en/beats/journalbeat/current/journalbeat-overview.html
            removed-by = Timo Gurr <tgurr@exherbo.org>
            removed-from = net
    openvas-cli/
        :0 1.4.5-r1
            comment = discontinued, partly replaced by gvm-tools
            commit-id = 667d6fa
            description = OpenVAS Commandline Interface Tools
            homepage = http://www.openvas.org
            removed-by = Timo Gurr <tgurr@exherbo.org>
            removed-from = net
    openvas-libraries/
        :0 9.0.3
            comment = replaced by gvm-libs
            commit-id = bc3b995
            description = OpenVAS libraries
            homepage = http://www.openvas.org
            removed-by = Timo Gurr <tgurr@exherbo.org>
            removed-from = net
    openvas-manager/
        :0 7.0.3
            comment = replaced by gvmd
            commit-id = b5a25a7
            description = OpenVAS Manager
            homepage = http://www.openvas.org
            removed-by = Timo Gurr <tgurr@exherbo.org>
            removed-from = net
    ospd/
        :0 21.4.4
            comment = has been merged into ospd-openvas
            commit-id = 70f65e6
            description = Open Scanner Protocol daemon
            homepage = https://pypi.org/project/ospd/
            removed-by = Timo Gurr <tgurr@exherbo.org>
            removed-from = net
net-apps/
    cclive/
        :0 0.9.3
            comment = Dead upstream, last release from 2013, depends on also abandoned quvi
            commit-id = 49f42ec
            description = Command line video download tool
            homepage = http://cclive.sourceforge.net/
            removed-by = Timo Gurr <tgurr@exherbo.org>
            removed-from = net
net-fs/
    openafs/
        :0 1.6.2-r2
            commit-id = 6e2faaa2
            description = OpenAFS is an open source implementation of the Andrew distributed file system (AFS)
            homepage = http://www.openafs.org/
            removed-by = Timo Gurr <tgurr@exherbo.org>
            removed-from = net
net-im/
    mcabber/
        :0 0.10.2-r1
            comment = Unmaintained, fails to build
            commit-id = d21a47b4
            description = A small Jabber console client.
            homepage = http://www.mcabber.com
            removed-by = Marvin Schmidt <marv@exherbo.org>
            removed-from = net
    minbif/
        :0 1.0.5
            comment = Fails to build, appears unused
            commit-id = 458e82b8
            description = Minbif is an IRC gateway to IM networks
            homepage = https://symlink.me/romain/minbif
            removed-by = Heiko Becker <heirecka@exherbo.org>
            removed-from = net
net-libs/
    avhttp/
        :0 2.9.9_p20150907
            comment = Dead upstream
            commit-id = 35621f29cbae
            description = HTTP client library based on Boost.Asio
            homepage = https://github.com/avplayer/avhttp
            removed-by = Marvin Schmidt <marv@exherbo.org>
            removed-from = net
    libpurple/
        :0 2.10.1
            comment = duplicate of net-im/pidgin
            commit-id = 1aafdd4
            description = libpurple has support for many commonly used instant messaging protocols
            homepage = http://developer.pidgin.im/wiki/WhatIsLibpurple
            removed-by = Ivan Dives <ivan.dives@gmail.com>
            removed-from = net
net-misc/
    ipsec-tools/
        :0 0.8.2-r1
            comment = Dead upstream, CVE-2016-10396, FTBFS with OpenSSL 1.1 (Debian has patches)
            commit-id = a932a456
            description = Tools for configuring and using IPSEC
            homepage = http://ipsec-tools.sourceforge.net
            removed-by = Timo Gurr <tgurr@exherbo.org>
            removed-from = net
    openswan/
        :0 2.6.37-r4
            comment = Unmaintained on Exherbo and vulnerable
            commit-id = b208344f
            description = A set of tools for doing L2TP/IPsec on Linux operating systems
            homepage = http://www.openswan.org/
            removed-by = Timo Gurr <tgurr@exherbo.org>
            removed-from = net
    tucan/
        :0 0.3.10-r1
            comment = Download and git repo hosting vanished with Google code
            commit-id = d638a1b5
            description = Free file sharing application designed for 1-Click Hosters
            homepage = http://tucaneando.com/
            removed-by = Heiko Becker <heirecka@exherbo.org>
            removed-from = net
    wicd/
        :0 1.7.4-r2
            comment = Dead
            commit-id = ea49e7fc
            description = Wired and wireless network manager
            homepage = https://launchpad.net/wicd/
            removed-by = Marvin Schmidt <marv@exherbo.org>
            removed-from = net
net-news/
    FlexGet/
        :0 1.0_p3122
            commit-id = 18f1dfa3
            description = Automate downloading or processing content (torrents, podcasts) from sources like RSS-feeds, html-pages, various sites and more
            homepage = http://flexget.com/
            removed-by = Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
            removed-from = net
net-nntp/
    sabnzbd+/
        :0 2.3.2
            comment = Unmaintained on Exherbo
            commit-id = 21e7e3fd
            description = SABnzbd is an open-source cross-platform binary newsreader
            homepage = https://sabnzbd.org
            removed-by = Timo Gurr <tgurr@exherbo.org>
            removed-from = net
net-remote/
    icaclient/
        :0 13.1.0.285639-r1
            comment = Outdated version which probably can't be downloaded anymore, depends on gstreamer:0.10
            commit-id = ea932ebc
            description = ICA Client for Citrix Presentation servers
            homepage = http://www.citrix.com/downloads/citrix-receiver/linux
            removed-by = Heiko Becker <heirecka@exherbo.org>
            removed-from = net
net-www/
    uzbl/
        :0 2012.05.14
            comment = Unmaintained, dead upstream
            commit-id = b5e37904
            description = Highly customisable minimal web browser
            homepage = https://www.uzbl.org/
            removed-by = Heiko Becker <heirecka@exherbo.org>
            removed-from = net
sys-apps/
    storaged/
        :0 2.6.3
            comment = storaged merged back with the udisks project using the original name udisks
            commit-id = d0c3a0bba59b8d1d4d32977fbd7bbb8481abf3a8
            description = Daemon, tools, libraries to access and manipulate disks and storage devices
            homepage = https://github.com/storaged-project/udisks
            removed-by = Timo Gurr <tgurr@exherbo.org>
            removed-from = net
sys-auth/
    pam-afs-session/
        :0 2.5
            commit-id = 2992f010
            description = PAM module for obtaining an AFS token.
            homepage = http://www.eyrie.org/~eagle/software/pam-afs-session/
            removed-by = Timo Gurr <tgurr@exherbo.org>
            removed-from = net
voip/
    murmur/
        :0 1.4.287
            comment = Renamed to voip/mumble-server
            commit-id = 29ef955
            description = Mumble is an open source, low-latency, high quality voice chat software
            homepage = https://mumble.info
            removed-by = Timo Gurr <tgurr@exherbo.org>
            removed-from = net
web-apps/
    Trac/
        :0 1.0.6
            commit-id = 6d0515e0
            description = An enhanced wiki and issue tracking system for software development projects
            homepage = https://pypi.org/project/Trac/
            removed-by = Timo Gurr <tgurr@exherbo.org>
            removed-from = net
    TracAccountManager/
        :0 0.4.4
            commit-id = e1489f0b
            description = Trac plugin to manage user accounts
            homepage = https://trac-hacks.org/wiki/AccountManagerPlugin
            removed-by = Timo Gurr <tgurr@exherbo.org>
            removed-from = net
